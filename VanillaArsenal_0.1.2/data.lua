require('prototypes.recipe')
require('prototypes.technology')
--
require('prototypes.item.cobblestone-wall_item')
require('prototypes.recipe.cobblestone-wall_recipe')
require('prototypes.entity.cobblestone-wall_entity')
require('prototypes.technology.cobblestone-wall_technology')
--
require('prototypes.entity.better-combat-bot_entity')
require('prototypes.entity.better-combat-bot-projectiles_entity')
require('prototypes.item.better-combat-capsules_item')
require('prototypes.recipe.better-combat-capsules_recipe')
--
