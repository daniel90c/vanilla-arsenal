data:extend(
{
{
    type = "item",
    name = "cobblestone-wall",
    icon = "__VanillaArsenal__/graphics/item/cobblestone-wall.png",
    icon_size = 32,
    --flags = {"goes-to-quickbar"},
    subgroup = "defensive-structure",
    order = "a[cobblestone-wall]-a[stone-wall]",
    place_result = "cobblestone-wall",
    stack_size = 100
  }
}
)