data:extend(
{
    {
        type = "technology",
        name = "cobblestone-wall",
        icon = "__base__/graphics/technology/stone-walls.png",
		icon_size = 128,
        effects =
        {
            {
                type = "unlock-recipe",
                recipe = "cobblestone-wall"
            },
        },
        --prerequisites = {"military", "automation"},
        unit =
        {
            count = 20,
            ingredients =
            {
                {"automation-science-pack", 1},
                --{"logistic-science-pack", 1},
                --{"military-science-pack", 1}
            },
            time = 10
        },
        upgrade = true,
        order = "e-l-a"
    }
}
)