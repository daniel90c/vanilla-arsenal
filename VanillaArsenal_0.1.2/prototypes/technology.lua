data:extend({
-- Weapon Shooting Speed
 {
    type = "technology",
    name = "weapon-shooting-speed-7",
    icon_size = 128,
    icon = "__base__/graphics/technology/weapon-shooting-speed-3.png",
    effects =
    {
      {
        type = "gun-speed",
        ammo_category = "bullet",
        modifier = 0.2
      },
      {
        type = "gun-speed",
        ammo_category = "shotgun-shell",
        modifier = 0.2
      },
      {
        type = "gun-speed",
        ammo_category = "cannon-shell",
        modifier = 0.3
      },
      {
        type = "gun-speed",
        ammo_category = "rocket",
        modifier = 0.3
      }
    },
    prerequisites = {"weapon-shooting-speed-6"},
    unit =
    {
      count_formula = "2^(L-7)*1000",
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"chemical-science-pack", 1},
        {"military-science-pack", 1},
        {"utility-science-pack", 1},
		{"space-science-pack", 1}
      },
      time = 60
    },
	max_level = "infinite",
    upgrade = true,
    order = "e-l-f"
  }
  })
data:extend({
 {
    type = "technology",
    name = "laser-turret-speed-8",
    icon_size = 128,
    icon = "__base__/graphics/technology/laser-turret-speed.png",
    effects =
    {
      {
        type = "gun-speed",
        ammo_category = "laser-turret",
        modifier = 0.2
      }
    },
    prerequisites = {"laser-turret-speed-7"},
    unit =
    {
      count_formula = "2^(L-8)*1000",
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"chemical-science-pack", 1},
        {"military-science-pack", 1},
        {"utility-science-pack", 1},
		    {"space-science-pack", 1}
      },
      time = 60
    },
	max_level = "infinite",
    upgrade = true,
    order = "e-n-n"
  },
})
 
--- Explosives damage from rampant arsenal!
data:extend({
{
    type = "technology",
    name = "vanilla-arsenal-stronger-explosives-1",
    icon_size = 128,
    icon = "__base__/graphics/technology/stronger-explosives-1.png",
        effects =
        {
            {
                type = "ammo-damage",
                ammo_category = "grenade",
                modifier = 0.10
            }
        },
                prerequisites = {"military-2"},
                unit =
                    {
                        count = 200*1,
                        ingredients =
                            {
                                {"automation-science-pack", 1},
                                {"logistic-science-pack", 1}
                            },
                        time = 30
                    },
                upgrade = true,
                order = "e-l-e"
            }
        }
        )
--- bullet Damage 

data:extend({
  {
    type = "technology",
    name = "vanilla-bullet-damage-1",
    icon_size = 128,
    icon = "__base__/graphics/technology/physical-projectile-damage-1.png",
      effects =
          {
             {
            type = "ammo-damage",
            ammo_category = "bullet",
            modifier = 0.05
            }
           },
    prerequisites = {"turrets", "military-2", "military-science-pack"},
    unit =
    {
      count = 200,
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"military-science-pack", 1},
       },
       time = 30
    },
    upgrade = true,
    order = "e-j-a"
  }
})
--
data:extend({
  {
    type = "technology",
    name = "vanilla-bullet-damage-2",
    icon_size = 128,
    icon = "__base__/graphics/technology/physical-projectile-damage-1.png",
      effects =
          {
             {
            type = "ammo-damage",
            ammo_category = "bullet",
            modifier = 0.05
            }
           },
    prerequisites = {"vanilla-bullet-damage-1"},
    unit =
    {
      count = 300,
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"military-science-pack", 1},
       },
       time = 20
    },
    upgrade = true,
    order = "e-j-a"
  }
})
--
data:extend({
  {
    type = "technology",
    name = "vanilla-bullet-damage-3",
    icon_size = 128,
    icon = "__base__/graphics/technology/physical-projectile-damage-1.png",
      effects =
          {
             {
            type = "ammo-damage",
            ammo_category = "bullet",
            modifier = 0.05
            }
           },
    prerequisites = {"vanilla-bullet-damage-2"},
    unit =
    {
      count = 300,
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"military-science-pack", 1},
       },
       time = 60
    },
    upgrade = true,
    order = "e-j-a"
  }
})
--
data:extend({
  {
    type = "technology",
    name = "vanilla-bullet-damage-4",
    icon_size = 128,
    icon = "__base__/graphics/technology/physical-projectile-damage-1.png",
      effects =
          {
             {
            type = "ammo-damage",
            ammo_category = "bullet",
            modifier = 0.05
            }
           },
    prerequisites = {"vanilla-bullet-damage-3"},
    unit =
    {
      count = 400,
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"military-science-pack", 1},
        --{"chemical-science-pack", 1}
       },
       time = 60
    },
    upgrade = true,
    order = "e-j-a"
  }
})
--
data:extend({
  {
    type = "technology",
    name = "vanilla-bullet-damage-5",
    icon_size = 128,
    icon = "__base__/graphics/technology/physical-projectile-damage-1.png",
      effects =
          {
             {
            type = "ammo-damage",
            ammo_category = "bullet",
            modifier = 0.05
            }
           },
    prerequisites = {"vanilla-bullet-damage-4"},
    unit =
    {
      count = 500,
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"military-science-pack", 1},
        {"chemical-science-pack", 1}
       },
       time = 90
    },
    upgrade = true,
    order = "e-j-a"
  }
})
--
data:extend({
  {
    type = "technology",
    name = "vanilla-bullet-damage-6",
    icon_size = 128,
    icon = "__base__/graphics/technology/physical-projectile-damage-1.png",
      effects =
          {
             {
            type = "ammo-damage",
            ammo_category = "bullet",
            modifier = 0.05
            }
           },
    prerequisites = {"vanilla-bullet-damage-5"},
    unit =
    {
      count = 600,
      ingredients =
      {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"military-science-pack", 1},
        {"chemical-science-pack", 1}
       },
       time = 90
    },
    upgrade = true,
    order = "e-j-a"
  }
})