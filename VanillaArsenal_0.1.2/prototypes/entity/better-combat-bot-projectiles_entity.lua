data.raw["projectile"]["destroyer-capsule"].
    action =
    {
      type = "direct",
      action_delivery =
      {
        type = "instant",
        target_effects =
        {
          type = "create-entity",
          show_in_tooltip = true,
          entity_name = "destroyer",
          offsets = {{0, 0}}
        }
      }
    }
	
	data.raw["projectile"]["distractor-capsule"].
    action =
    {
      type = "direct",
      action_delivery =
      {
        type = "instant",
        target_effects =
        {
          {
            type = "create-entity",
            show_in_tooltip = true,
            entity_name = "distractor",
            offsets = {{0, 0}}
          }
        }
      }
    }
	