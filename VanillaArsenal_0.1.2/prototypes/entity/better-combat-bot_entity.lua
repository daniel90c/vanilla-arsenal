data.raw["combat-robot"]["defender"].
    flags = {"placeable-player", "player-creation", "placeable-off-grid", "not-on-map"}
data.raw["combat-robot"]["defender"].
    minable = {mining_time = 0.05, result = "defender-capsule"}
data.raw["combat-robot"]["defender"].
	time_to_live = 4294967295

		data.raw["combat-robot"]["destroyer"].
    flags = {"placeable-player", "player-creation", "placeable-off-grid", "not-on-map"}
data.raw["combat-robot"]["destroyer"].
    minable = {mining_time = 0.05, result = "destroyer-capsule"}
data.raw["combat-robot"]["destroyer"].
	time_to_live = 4294967295