data:extend(
{
    {
        type = "wall",
        name = "cobblestone-wall",
        icon = "__VanillaArsenal__/graphics/item/cobblestone-wall.png",
        icon_size = 32,
        flags = {"placeable-neutral", "player-creation"},
        collision_box = {{-0.29, -0.29}, {0.29, 0.29}},
        selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
        minable = {mining_time = 0.5, result = "cobblestone-wall"},
        fast_replaceable_group = "wall",
        max_health = 200,
        repair_speed_modifier = 2,
        corpse = "wall-remnants",
        repair_sound = { filename = "__base__/sound/manual-repair-simple.ogg" },
        mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
        vehicle_impact_sound =  { filename = "__base__/sound/car-stone-impact.ogg", volume = 1.0 },
        -- this kind of code can be used for having walls mirror the effect
        -- there can be multiple reaction items
        --attack_reaction =
        --{
          --{
            ---- how far the mirroring works
            --range = 2,
            ---- what kind of damage triggers the mirroring
            ---- if not present then anything triggers the mirroring
            --damage_type = "physical",
            ---- caused damage will be multiplied by this and added to the subsequent damages
            --reaction_modifier = 0.1,
            --action =
            --{
              --type = "direct",
              --action_delivery =
              --{
                --type = "instant",
                --target_effects =
                --{
                  --type = "damage",
                  ---- always use at least 0.1 damage
                  --damage = {amount = 0.1, type = "physical"}
                --}
              --}
            --},
          --}
        --},
        connected_gate_visualization =
        {
          filename = "__core__/graphics/arrows/underground-lines.png",
          priority = "high",
          width = 64,
          height = 64,
          scale = 0.5
        },
        resistances =
        {
          {
            type = "physical",
            decrease = 3,
            percent = 15
          },
          {
            type = "impact",
            decrease = 45,
            percent = 45
          },
          {
            type = "explosion",
            decrease = 10,
            percent = 25
          },
          {
            type = "fire",
            percent = 90
          },
          {
            type = "laser",
            percent = 70
          }
        },
        pictures =
        {
          single =
          {
            layers =
            {
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-single.png",
                priority = "extra-high",
                width = 22,
                height = 42,
                shift = {0, -0.15625}
              },
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-single-shadow.png",
                priority = "extra-high",
                width = 47,
                height = 32,
                shift = {0.359375, 0.5},
                draw_as_shadow = true
              }
            }
          },
          straight_vertical =
          {
            {
              layers =
              {
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-vertical-1.png",
                  priority = "extra-high",
                  width = 22,
                  height = 42,
                  shift = {0, -0.15625}
                },
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-vertical-shadow.png",
                  priority = "extra-high",
                  width = 47,
                  height = 60,
                  shift = {0.390625, 0.625},
                  draw_as_shadow = true
                }
              }
            },
            {
              layers =
              {
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-vertical-2.png",
                  priority = "extra-high",
                  width = 22,
                  height = 42,
                  shift = {0, -0.15625}
                },
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-vertical-shadow.png",
                  priority = "extra-high",
                  width = 47,
                  height = 60,
                  shift = {0.390625, 0.625},
                  draw_as_shadow = true
                }
              }
            },
            {
              layers =
              {
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-vertical-3.png",
                  priority = "extra-high",
                  width = 22,
                  height = 42,
                  shift = {0, -0.15625}
                },
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-vertical-shadow.png",
                  priority = "extra-high",
                  width = 47,
                  height = 60,
                  shift = {0.390625, 0.625},
                  draw_as_shadow = true
                }
              }
            }
          },
          straight_horizontal =
          {
            {
              layers =
              {
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-horizontal-1.png",
                  priority = "extra-high",
                  width = 32,
                  height = 42,
                  shift = {0, -0.15625}
                },
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-horizontal-shadow.png",
                  priority = "extra-high",
                  width = 59,
                  height = 32,
                  shift = {0.421875, 0.5},
                  draw_as_shadow = true
                }
              }
            },
            {
              layers =
              {
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-horizontal-2.png",
                  priority = "extra-high",
                  width = 32,
                  height = 42,
                  shift = {0, -0.15625}
                },
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-horizontal-shadow.png",
                  priority = "extra-high",
                  width = 59,
                  height = 32,
                  shift = {0.421875, 0.5},
                  draw_as_shadow = true
                }
              }
            },
            {
              layers =
              {
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-horizontal-3.png",
                  priority = "extra-high",
                  width = 32,
                  height = 42,
                  shift = {0, -0.15625}
                },
                {
                  filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-straight-horizontal-shadow.png",
                  priority = "extra-high",
                  width = 59,
                  height = 32,
                  shift = {0.421875, 0.5},
                  draw_as_shadow = true
                }
              }
            }
          },
          corner_right_down =
          {
            layers =
            {
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-corner-right-down.png",
                priority = "extra-high",
                width = 27,
                height = 42,
                shift = {0.078125, -0.15625}
              },
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-corner-right-down-shadow.png",
                priority = "extra-high",
                width = 53,
                height = 61,
                shift = {0.484375, 0.640625},
                draw_as_shadow = true
              }
            }
          },
          corner_left_down =
          {
            layers =
            {
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-corner-left-down.png",
                priority = "extra-high",
                width = 27,
                height = 42,
                shift = {-0.078125, -0.15625}
              },
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-corner-left-down-shadow.png",
                priority = "extra-high",
                width = 53,
                height = 60,
                shift = {0.328125, 0.640625},
                draw_as_shadow = true
              }
            }
          },
          t_up =
          {
            layers =
            {
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-t-down.png",
                priority = "extra-high",
                width = 32,
                height = 42,
                shift = {0, -0.15625}
              },
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-t-down-shadow.png",
                priority = "extra-high",
                width = 71,
                height = 61,
                shift = {0.546875, 0.640625},
                draw_as_shadow = true
              }
            }
          },
          ending_right =
          {
            layers =
            {
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-ending-right.png",
                priority = "extra-high",
                width = 27,
                height = 42,
                shift = {0.078125, -0.15625}
              },
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-ending-right-shadow.png",
                priority = "extra-high",
                width = 53,
                height = 32,
                shift = {0.484375, 0.5},
                draw_as_shadow = true
              }
            }
          },
          ending_left =
          {
            layers =
            {
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-ending-left.png",
                priority = "extra-high",
                width = 27,
                height = 42,
                shift = {-0.078125, -0.15625}
              },
              {
                filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-ending-left-shadow.png",
                priority = "extra-high",
                width = 53,
                height = 32,
                shift = {0.328125, 0.5},
                draw_as_shadow = true
              }
            }
         -- },
          --water_connection_patch =
          --{
            --sheets =
            --{
             -- {
               -- filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-patch.png",
             --   priority = "extra-high",
             --  width = 52,
             --   height = 68,
             -- shift = util.by_pixel(0, -2)
             -- },
             -- {
               -- filename = "__VanillaArsenal__/graphics/entity/cobblestone-wall/wall-patch-shadow.png",
                --priority = "extra-high",
                --draw_as_shadow = true,
                --width = 74,
                --height = 96,
                --shift = util.by_pixel(6, 13)
             -- }
            --}
          }
    }
    }
    }
)