# Vanilla Arsenal Factorio Lua mod

![](VanillaArsenal_0.1.2/thumbnail.png) 

## Vanilla additions to provide better *arsenal* to deal with the natives.

#### Latest version: 

---

#### *Version: 0.17.2*

Changes:

    - Added new bullet damage for military science research levels 1 to 6
    - New Language: es-ES spanish translation

---

#### *Version: 0.17.1*

Changes:

    - initial release.
    - New Tech: Granade Damage
    - New Tech: Shooting Speed Infinite research
    - Modified tech: military science 2 now enables posion capsules